#!/usr/bin/env python3

# use credeface(1) (backed by pass(1)) to find a password

# NB: look at dosed() below, and its callers, to find how to
# manipulate host and user names, as well where to look in pass(1)'s
# tree

# XXX TODO:
# 1. allow --exit to kill off git credential-cache

import sys

CREDEFACECMD = "credeface"      # default command used for accessing cache
SEDPLACE = "~/.credeface/place" # where sed scripts are stored

# filled in in main()
credefacecmd = ""
sedplace = ""
cmdname = ""

def mport(modstring):
    """try to import a module named MODSTRING, printing an error
    message and aborting on failure.  (note that module sys must
    already be imported.)"""
    try:
        eval(compile("import {}".format(modstring), "foo.c", "single"), globals())
    except ImportError:
        sys.stderr.write("python error: unable to import required module \"{}\"\n".format(modstring))
        sys.exit(1)

def doimports():
    try:    
        import sys
    except ImportError:
        print("python error: unable to import \"sys\"")
        raise

    # module subprocess: http://stackoverflow.com/a/89243/1527747
    mport("os")
    mport("re")
    mport("subprocess")
    mport("argparse")

# from Jay, stackexchange, 18 Dec 2008 (last edit 10 Nov 2017 by
# mar77i): https://stackoverflow.com/a/377028
def which(program):
    import os
    def is_exe(fpath):
        return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None

def popen(cmd, stdin=None, stdout=None, stderr=None):
    """open a pipe, complaining in the event of a failure"""
    # PATTERN to ignore spaces inside quotes:
    # http://stackoverflow.com/q/2785755/1527747
    PATTERN = re.compile(r'''((?:[^ "']|"[^"]*"|'[^']*')+)''')
    # now, get rid of any stray quotes.  faster way?
    list = PATTERN.split(cmd)[1::2]
    dmc = []
    for i in list:
        dmc.append(re.sub('[\'"]', '', i))
    if args.commands:
        sys.stderr.write("%s\n" % " ".join(dmc))
    try:
        po = subprocess.Popen(dmc, stdin=stdin, stdout=stdout, stderr=stderr, text=True)
    except OSError:
        sys.stderr.write("unable to exec {}.  is it installed?\n".format(cmd.split()[0]))
        sys.exit(1)
    return po

def getpass(host, username):
    """using the namespace args, get the password; return \"None\" if no
password found"""
    gc = popen("%s get" % credefacecmd, stdout=subprocess.PIPE)
    password = None
    for line in gc.stdout:
        line = line.rstrip()    # get rid of trailing newline
        if not password:
            password = line
        else:
            sys.stderr.write("host \"%s\", username \"%s\": found multiple passwords\n" % host, username)
            sys.exit(2)
    gc.wait()
    if not gc.returncode in [0,1]:
        sys.stderr.write("%s: credefacecmd exited with status %d\n" % (cmdname, gc.returncode))
        sys.exit(gc.returncode)
    return password
                
def setpass(host, username, password):
    """add a binding to the credential manager."""

    cr = popen("%s store" % credefacecmd, stdin=subprocess.PIPE)
    cr.stdin.write("%s\n" % password)
    cr.stdin.close()
    cr.wait()
    if not cr.returncode == 0:
        sys.stderr.write("%s: credefacecmd exited with status %d\n" % (cmdname, cr.returncode))
        sys.exit(2)

def dosed(string, basename):
    """pass STRING through SEDPLACE/BASENAME, returning the result"""

    scriptfile = "%s/%s" % (sedplace, basename)
    # does the scriptfile exist?
    if not os.path.isfile(scriptfile):
        return string

    sed = popen("sed -f %s" % scriptfile, stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    sed.stdin.write("%s\n" % string)
    sed.stdin.close()
    answer = None
    for line in sed.stdout:
        line = line.rstrip()    # get rid of trailing newline
        if answer == None:
            answer = line
        else:
            sys.stderr.write("multiple results from \"sed -f %s/%s\"\n" % scriptfile)
            sys.exit(2)
    sed.wait()
    if not sed.returncode == 0:
        sys.stderr.write("%s: sed exited with status %d\n" % (cmdname, sed.returncode))
        sys.exit(2)
    if answer == None:
        sys.stderr.write("no result from (basically) \"echo %s | sed -f %s\"\n" % (string, scriptfile))
        sys.exit(2)
    return answer

def getbackpass(host, username):
    # find password, if possible, using pass(1)

    # first, allow some re-writing, using sed
    phost = dosed(host, "host.sed")
    pusername = dosed(username, "username.sed")
    # now, give the user the ability to change the location we look
    # for in pass(1)'s tree
    ppasst = dosed("%s/%s" % (host, username), "passpath.sed")

    # now, invoke pass(1).  *this* may cause the user to be asked to
    # supply a git passphrase

    gc = popen("pass %s/%s" % (phost, pusername), stdout=subprocess.PIPE)
    password = None
    for line in gc.stdout:
        line = line.rstrip()
        if password == None:
            password = line
    if password == None:
        sys.stderr.write("%s: no password returned by (basically) \"pass ... | head -1\"\n" % cmdname)
        sys.exit(2)
    gc.wait()               # should end...
    if not gc.returncode == 0:
        sys.stderr.write("%s: pass(1) exited with status %d\n" % (cmdname, gc.returncode))
        sys.exit(2)
    return password

def main(argv):
    global credefacecmd           # global scope
    global sedplace
    global cmdname
    global args

    doimports()

    cmdname = os.path.basename(sys.argv[0])

    parser = argparse.ArgumentParser(description="look for cached credentials, backed up by pass(1)")
    parser.add_argument('--username', action="store", required=True,
                        help="retrieve the password of this username (and host)")
    parser.add_argument('--host', action="store", required=True,
                        help="retrieve the password for this host (and username)")
    parser.add_argument('--yes-tty', action="store_true",
                        help="[dangerous!] allow secrets to be printed on the terminal", default=False)
    # XXX add option to enter passwords from terminal
    parser.add_argument('--timeout', action="store", type=int,
                        help="number of seconds to hold secrets", default=60*24*7)

    # some not-very-used arguments
    parser.add_argument('--commands', action='store_const', const=True, default=False,
                        help='print out (to stderr) shell commands as they are executed (also passed to credeface)')
    parser.add_argument('--credefacecmd', action="store",
                        help="command used to access git credential-cache",
                        default=CREDEFACECMD)
    parser.add_argument('--sedplace', action="store",
                        help="where to look for sed files",
                        default=SEDPLACE)
    
    args = parser.parse_args()

    # validate args: 1) check tty if !yes-tty;

    # 1) check tty if !yes-tty
    if not args.yes_tty:
       if os.isatty(sys.stdout.fileno()): # https://stackoverflow.com/q/30935706
          sys.stderr.write("cannot display secrets on the terminal\n")
          sys.exit(1)

    # re-write, with the full command to invoke credeface
    credefacecmd = "%s --host %s --username %s --timeout %s %s" % (args.credefacecmd,
                                                                args.host,
                                                                args.username,
                                                                args.timeout,
                                                                   "--commands" if args.commands else "")
    # where sed scripts (see dosed() above) may exist
    sedplace = os.path.expanduser(args.sedplace) # expand tilde

    # first, look in the cache
    pw = getpass(args.host, args.username)
    if pw:
        # found it!
        sys.stdout.write("%s\n" % pw)
        sys.exit(0)

    # okay, the password is not in the cache. find it in pass(1)
    pw = getbackpass(args.host, args.username)
    if pw == None:
        sys.stderr.write("unable to find password for host \"%s\", username \"%s\"\n" % (args.host,
                                                                                         args.username))
        sys.exit(1)

    # now, stash this expensively-obtained in the credential cache.
    # we store in credeface at the *original* place (w.r.t. host,
    # username), since that is where we looked for it in the cache.
    setpass(args.host, args.username, pw)

    # now, get it, from that same place, to make sure we are really
    # loading the cache
    pw = getpass(args.host, args.username)
    if pw:
        sys.stdout.write("%s\n" % pw)
        sys.exit(0)
    else:
        sys.stderr.write("%s: stored password for host \"%s\" and username \"%s\" lost between store and get" % (args.host, args.username))
        sys.exit(2)
            
if __name__ == "__main__":
    import sys
    main(sys.argv)
